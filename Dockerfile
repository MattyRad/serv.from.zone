FROM rust:1.75 as build

WORKDIR /app

COPY . .

RUN cargo build --release

FROM debian:latest

COPY --from=build /app/target/release/sfz /usr/local/bin/sfz

ENTRYPOINT ["sfz"]
